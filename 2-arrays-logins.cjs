let arrayLogins = [{ "id": 1, "first_name": "Valera", "last_name": "Pinsent", "email": "vpinsent0@google.co.jp", "gender": "Male", "ip_address": "253.171.63.171" },
{ "id": 2, "first_name": "Kenneth", "last_name": "Hinemoor", "email": "khinemoor1@yellowbook.com", "gender": "Polygender", "ip_address": "50.231.58.150" },
{ "id": 3, "first_name": "Roman", "last_name": "Sedcole", "email": "rsedcole2@addtoany.com", "gender": "Genderqueer", "ip_address": "236.52.184.83" },
{ "id": 4, "first_name": "Lind", "last_name": "Ladyman", "email": "lladyman3@wordpress.org", "gender": "Male", "ip_address": "118.12.213.144" },
{ "id": 5, "first_name": "Jocelyne", "last_name": "Casse", "email": "jcasse4@ehow.com", "gender": "Agender", "ip_address": "176.202.254.113" },
{ "id": 6, "first_name": "Stafford", "last_name": "Dandy", "email": "sdandy5@exblog.jp", "gender": "Female", "ip_address": "111.139.161.143" },
{ "id": 7, "first_name": "Jeramey", "last_name": "Sweetsur", "email": "jsweetsur6@youtube.com", "gender": "Genderqueer", "ip_address": "196.247.246.106" },
{ "id": 8, "first_name": "Anna-diane", "last_name": "Wingar", "email": "awingar7@auda.org.au", "gender": "Agender", "ip_address": "148.229.65.98" },
{ "id": 9, "first_name": "Cherianne", "last_name": "Rantoul", "email": "crantoul8@craigslist.org", "gender": "Genderfluid", "ip_address": "141.40.134.234" },
{ "id": 10, "first_name": "Nico", "last_name": "Dunstall", "email": "ndunstall9@technorati.com", "gender": "Female", "ip_address": "37.12.213.144" }];



// 1. Find all people who are Agender
let agenderPeople = arrayLogins.filter((people) => {
    return people.gender === "Agender";
});

console.log(agenderPeople);

// 2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
let splitIpAddress = arrayLogins.map((people) => {
    let ipAddress = people.ip_address;
    let splitAddress = ipAddress.split('.');
    return result = splitAddress.map((address) => {
        return Number(address);
    })
})
console.log(splitIpAddress);

// 3. Find the sum of all the second components of the ip addresses.
let sumofSecondIpAddress = splitIpAddress.reduce((accumulater, currentValue) => {
    return accumulater + currentValue[1];
}, 0);

console.log(sumofSecondIpAddress);

// 3.  Find the sum of all the fourth components of the ip addresses.
let sumOffourthIpAddress = splitIpAddress.reduce((accumulater, currentValue) => {
    return accumulater + currentValue[3];
}, 0);
console.log(sumOffourthIpAddress);

// 4. Compute the full name of each person and store it in a new key (full_name or something) for each person.
let fullName = arrayLogins.map((names) => {
    names.full_name = names.first_name + " " + names.last_name;
    return names;
});
console.log(fullName);
//console.log(arrayLogins);

// 5. Filter out all the .org emails
let orgMails = arrayLogins.filter((elements) => {
    return elements.email.endsWith('.org');
});
console.log(orgMails);

// 6. Calculate how many .org, .au, .com emails are there
/*let countEmails =arrayLogins.reduce((accumulater,currentValue)=> {
      if(accumulater[currentValue.email]){

      }
},{})*/


// 7. Sort the data in descending order of first name
let sortFirstName = arrayLogins.sort((first, second) => {
    if (first.first_name > second.first_name) {
        return -1;
    }
    else if (first.first_name < second.first_name) {
        return 1;
    }
    else {
        return 0;
    }
})

console.log(sortFirstName);


